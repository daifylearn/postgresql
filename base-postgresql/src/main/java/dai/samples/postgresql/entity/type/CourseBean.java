package dai.samples.postgresql.entity.type;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author daify
 * @date 2021-05-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseBean {

    private String course;

    private int grade;


    public static CourseBean fromString(String s) {
        CourseBean courseBean = JSON.parseObject(s, CourseBean.class);
        return courseBean;
    }
}