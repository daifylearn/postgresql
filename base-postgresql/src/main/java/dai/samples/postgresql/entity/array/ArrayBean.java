package dai.samples.postgresql.entity.array;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author daify
 * @date 2021-05-24
 */
@Data
@Entity
@TypeDefs({
        @TypeDef(name = "string-array",typeClass = StringArrayType.class)
})
public class ArrayBean {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Type(type = "string-array")
    @Column(columnDefinition = "text[]")
    private String[] authorName;
}
