package dai.samples.postgresql.entity.converter;

import dai.samples.postgresql.config.converter.ItemBeanConverter;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author daify
 * @date 2021-05-24
 */
@Data
@Entity
public class MoneyBean {

    @Id
    @Column
    private String uuid;

    @NotNull
    @Convert(
            converter = ItemBeanConverter.class,
            disableConversion = false
    )
    @Column()
    private ItemBean itemBean;
}
