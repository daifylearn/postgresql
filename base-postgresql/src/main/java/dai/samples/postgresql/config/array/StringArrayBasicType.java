package dai.samples.postgresql.config.array;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;

/**
 * @author daify
 * @date 2021-05-24
 */
public class StringArrayBasicType extends AbstractSingleColumnStandardBasicType<String[]> {

    /**
     * sql 和 java类型描述的映射
     */
    public StringArrayBasicType() {
        super(ArraySqlTypeDescriptor.INSTANCE, StringArrayTypeDescriptor.INSTANCE);

    }

    /**
     * 数据类型的名称
     * @return
     */
    @Override
    public String getName() {
        return "string-array";
    }

    /**
     * 是否向基本类型中注入java类型
     * @return
     */
    @Override
    protected boolean registerUnderJavaType() {
        return true;
    }

}