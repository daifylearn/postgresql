package dai.samples.postgresql.repository;

import dai.samples.postgresql.entity.converter.MoneyBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author daify
 * @date 2021-05-24
 */
public interface MoneyBeanRepository extends JpaRepository<MoneyBean,String> {
}
