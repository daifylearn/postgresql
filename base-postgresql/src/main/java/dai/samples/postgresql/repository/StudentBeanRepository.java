package dai.samples.postgresql.repository;

import dai.samples.postgresql.entity.type.StudentBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author daify
 * @date 2021-05-24
 */
public interface StudentBeanRepository extends JpaRepository<StudentBean,String> {
}
