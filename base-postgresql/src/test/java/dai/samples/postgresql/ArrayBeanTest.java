package dai.samples.postgresql;

import com.alibaba.fastjson.JSON;
import dai.samples.postgresql.entity.array.ArrayBean;
import dai.samples.postgresql.repository.ArrayBeanRepository;
import dai.samples.postgresql.service.ArrayBeanServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author daifeiya
 * @date 2021/5/10
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PostgresqlApplication.class)
public class ArrayBeanTest {

    @Autowired
    private EntityManager em;

    @Autowired
    ArrayBeanServiceImpl arrayBeanService;

    @Autowired
    ArrayBeanRepository arrayBeanRepository;


    @Test
    public void save() {
        ArrayBean bean = new ArrayBean();
        bean.setId(UUID.randomUUID().toString());
        bean.setName("test");
        bean.setAuthorName(new String[]{"a","b"});
        arrayBeanRepository.save(bean);
    }

    @Test
    public void get() {
        List<ArrayBean> all = arrayBeanRepository.findAll();
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    public void findByAuthorNameIn() {
        List<ArrayBean> all = arrayBeanRepository.findByAuthorName(new String[]{"a","b"});
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    public void findByAuthorNameIn2() {
        String[] name = {"amit", "rahul", "surya"};
        String result = String.join(",", name);
        System.out.println(result);
        List<ArrayBean> all = arrayBeanRepository.findByAuthorNameIn2(Arrays.asList(new String[]{"a","b"}));
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    public void findByAuthorNameIn3() {
        List<ArrayBean> all = arrayBeanRepository.findByAuthorNameIn2("a,c");
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    public void findByAuthorNameIn4() {
        List<String> query = new ArrayList<>();
        query.add("f");
        query.add("c");
        List all = arrayBeanService.findArrayBeanByAuthorNameIn(query);
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    public void demoQuery() {
        List resultList = em.createNativeQuery("select CAST(ARRAY[1,2,3] as int[]) = ARRAY[1,2,3]").getResultList();
        System.out.println("select CAST(ARRAY[1,2,3] as int[]) = CAST(ARRAY[1,2,3] result");
        System.out.println(JSON.toJSONString(resultList));

        List resultList1 = em.createNativeQuery("select CAST(ARRAY[1,2,3] as int[]) <> ARRAY[1,2,4]").getResultList();
        System.out.println("select CAST(ARRAY[1,2,3]  as int[])  <> ARRAY[1,2,4] result");
        System.out.println(JSON.toJSONString(resultList1));

        List resultList2 = em.createNativeQuery("select CAST(ARRAY[1,2,3] as int[]) < ARRAY[1,2,4]").getResultList();
        System.out.println("select CAST(ARRAY[1,2,3] as int[]) < ARRAY[1,2,4] result");
        System.out.println(JSON.toJSONString(resultList2));

        List resultList3 = em.createNativeQuery("select CAST(ARRAY[1,4,3] as int[]) > ARRAY[1,2,4]").getResultList();
        System.out.println("select CAST(ARRAY[1,4,3] as int[]) > ARRAY[1,2,4] result");
        System.out.println(JSON.toJSONString(resultList3));

        List resultList4 = em.createNativeQuery("select CAST(ARRAY[1,2,3] as int[]) <= ARRAY[1,2,3]").getResultList();
        System.out.println("select CAST(ARRAY[1,2,3] as int[]) <= ARRAY[1,2,3] result");
        System.out.println(JSON.toJSONString(resultList4));

        List resultList5 = em.createNativeQuery("select CAST(ARRAY[1,4,3] as int[]) >= ARRAY[1,4,3]").getResultList();
        System.out.println("select CAST(ARRAY[1,4,3] as int[]) >= ARRAY[1,4,3] result");
        System.out.println(JSON.toJSONString(resultList5));

        List resultList6 = em.createNativeQuery("select CAST(ARRAY[1,4,3] as int[]) @> ARRAY[3,1]").getResultList();
        System.out.println("select CAST(ARRAY[1,4,3] as int[]) @> ARRAY[3,1] result");
        System.out.println(JSON.toJSONString(resultList6));

        List resultList7 = em.createNativeQuery("select CAST(ARRAY[2,7] as int[]) <@ ARRAY[1,7,4,2,6]").getResultList();
        System.out.println("select CAST(ARRAY[2,7] as int[]) <@ ARRAY[1,7,4,2,6] result");
        System.out.println(JSON.toJSONString(resultList7));

        List resultList8 = em.createNativeQuery("select CAST(ARRAY[1,4,3] as int[]) && ARRAY[2,1]").getResultList();
        System.out.println("select CAST(ARRAY[1,4,3] as int[]) && ARRAY[2,1] result");
        System.out.println(JSON.toJSONString(resultList8));
    }
}