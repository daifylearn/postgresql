package dai.samples.postgresql;

import com.alibaba.fastjson.JSON;
import dai.samples.postgresql.entity.converter.ItemBean;
import dai.samples.postgresql.entity.converter.MoneyBean;
import dai.samples.postgresql.repository.MoneyBeanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * @author daifeiya
 * @date 2021/5/10
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PostgresqlApplication.class)
public class MoneyBeanRepositoryTest {

    @Autowired
    MoneyBeanRepository beanRepository;

    @Test
    public void save() {
        MoneyBean bean = new MoneyBean();
        bean.setUuid(UUID.randomUUID().toString());
        ItemBean itemBean = new ItemBean();
        itemBean.setUnit(100);
        itemBean.setValue(new BigDecimal("15"));
        bean.setItemBean(itemBean);
        beanRepository.save(bean);

    }

    @Test
    public void get() {
        List<MoneyBean> all = beanRepository.findAll();
        System.out.println(JSON.toJSONString(all));
    }
}