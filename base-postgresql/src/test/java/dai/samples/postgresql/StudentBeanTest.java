package dai.samples.postgresql;

import com.alibaba.fastjson.JSON;
import dai.samples.postgresql.entity.type.CourseBean;
import dai.samples.postgresql.entity.type.StudentBean;
import dai.samples.postgresql.repository.StudentBeanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

/**
 * @author daifeiya
 * @date 2021/5/10
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PostgresqlApplication.class)
public class StudentBeanTest {

    @Autowired
    StudentBeanRepository studentBeanRepository;


    @Test
    public void save() {
        StudentBean bean = new StudentBean();
        bean.setId(UUID.randomUUID().toString());
        bean.setName("test");
        CourseBean itemBean = new CourseBean();
        itemBean.setCourse("总分");
        itemBean.setGrade(15);
        bean.setCourseBean(itemBean);
        studentBeanRepository.save(bean);
    }

    @Test
    public void get() {
        List<StudentBean> all = studentBeanRepository.findAll();
        System.out.println(JSON.toJSONString(all));
    }
}
